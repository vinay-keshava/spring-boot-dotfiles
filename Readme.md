1.spring.jpa.hibernate.ddl-auto=update

https://stackoverflow.com/questions/42135114/how-does-spring-jpa-hibernate-ddl-auto-property-exactly-work-in-spring


2. @SpringBootApplication

https://docs.spring.io/spring-boot/docs/2.0.x/reference/html/using-boot-using-springbootapplication-annotation.html


3. when using an sql reserved words in column names or database there raises an error to SQLRESERVED words to prevent this we use the below syntax in application.properties, this can be either solved by escape sequences or putting the below line 

spring.jpa.properties.hibernate.globally_quoted_identifiers=true

https://www.chrouki.com/posts/escape-sql-reserved-keywords-jpa-hibernate/



4. Swagger UI with OpenAPI
https://springdoc.org/v2/
using the updated dependency
https://stackoverflow.com/questions/74701738/spring-boot-3-springdoc-openapi-ui-doesnt-work
